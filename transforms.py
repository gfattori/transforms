# Basics transforms
# gfattori 15.06.2023

import numpy as np

def mapPoints(MappingMatrix: np.array, Points: np.array ) :
    PointsHomogen = np.c_[ Points, np.ones(Points.shape[0]) ]
    MappedPoints = np.zeros(PointsHomogen.shape)
    PointIdx = 0
    for row in PointsHomogen:
        MappedPoints[PointIdx,:] = np.dot( MappingMatrix , row.T )
        PointIdx = PointIdx +1
    return np.delete(MappedPoints, 3, 1)

def map4x4TransformToNewOrigin(TransformMatrix: np.array, NewOrigin: np.array):
    M = np.eye(4)
    NewOriginTranslation = mapPoints(TransformMatrix, NewOrigin)
    DeltaNewOrigin = NewOriginTranslation - NewOrigin
    M[:3,:3] = TransformMatrix[:3,:3]
    M[:3,3] = DeltaNewOrigin[0,0:4]
    return M

def invert4x4Transform(M):
    Minv = np.eye(4)
    Minv[0:3,0:3] = M[0:3,0:3].T
    Minv[0:3,3]=-np.dot(M[0:3,0:3].T,M[0:3,3].T)
    return  Minv

def new4x4Transform(TranslationsMM: np.array, RotationsDEG: np.array) :
    Rrad = np.deg2rad(RotationsDEG)
    Rx = np.array([[1, 0, 0],
          [0, np.cos(Rrad[0]), -np.sin(Rrad[0])],
          [0, np.sin(Rrad[0]), np.cos(Rrad[0])]])
    Ry = np.array([[np.cos(Rrad[1]), 0, np.sin(Rrad[1])],
          [0, 1 , 0],
          [-np.sin(Rrad[1]), 0, np.cos(Rrad[1])]])  
    Rz = np.array([[np.cos(Rrad[2]), -np.sin(Rrad[2]), 0],
          [np.sin(Rrad[2]), np.cos(Rrad[2]), 0],
          [0, 0, 1]])
    R = np.dot(Rz, np.dot(Ry,Rx))
    MappingMatrix = np.eye(4)
    MappingMatrix[0:3,0:3] = R
    MappingMatrix[0:3,3] = TranslationsMM
    return MappingMatrix

def getEulersFrom4x4(M: np.array):
    R=M[:3,:3]
    twoSolutions = False
    
    if R[2,0] != 1 and R[2,0] != -1 :
        
        Ry1 = -np.arcsin(R[2,0])
        Ry2 = np.pi - Ry1
    
        Rx1 = np.arctan2(R[2,1]/np.cos(Ry1), R[2,2]/np.cos(Ry1))
        Rx2 = np.arctan2(R[2,1]/np.cos(Ry2), R[2,2]/np.cos(Ry2))
    
        Rz1 = np.arctan2(R[1,0]/np.cos(Ry1), R[0,0]/np.cos(Ry1))
        Rz2 = np.arctan2(R[1,0]/np.cos(Ry2), R[0,0]/np.cos(Ry2))
        twoSolutions = True
        
    else:
        Rz1 = 0
        
        if R[2,0] == -1 :
            Ry1 = np.pi/2
            Rx1 = Rz1 + np.arctan2(R[0,1],R[0,2])
        else :
            Ry1 = -np.pi/2
            Rx1 = -Rz1 + np.arctan2(-R[0,1],-R[0,2])
        twoSolutions = False

    if twoSolutions == True:
        Rot1= [np.rad2deg(Rx1), np.rad2deg(Ry1), np.rad2deg(Rz1)];
        Rot2= [np.rad2deg(Rx2), np.rad2deg(Ry2), np.rad2deg(Rz2)];
        ReturnRot = np.array([Rot1,Rot2])
    else:
        Rot1= [np.rad2deg(Rx1), np.rad2deg(Ry1), np.rad2deg(Rz1)];
        ReturnRot = np.array([Rot1])
    
    return ReturnRot



