# Basics points registration
# gfattori 15.06.2023

import numpy as np

from math import sqrt
# Implements Kabsch algorithm - best fit.
# Supports scaling (umeyama)
# Compares well to SA results for the same data.
# Input:
#     Nominal  A Nx3 matrix of points
#     Measured B Nx3 matrix of points
# Returns s,R,t
# s = scale B to A
# R = 3x3 rotation matrix (B to A)
# t = 3x1 translation vector (B to A)
def rigid_transform_3D(A, B) :
#scale):
    assert len(A) == len(B)

    N = A.shape[0];  # total points

    centroid_A = np.mean(A, axis=0)
    centroid_B = np.mean(B, axis=0)

    # center the points
    AA = A - np.tile(centroid_A, (N, 1))
    BB = B - np.tile(centroid_B, (N, 1))
        
    # dot is matrix multiplication for array
#    if scale:
#        H = np.transpose(AA) * BB / N
#    else:
    H = np.dot(AA.T,BB) 
    U, S, Vt = np.linalg.svd(H)

    R = np.dot(Vt.T, U.T)

    # special reflection case
    if np.linalg.det(R) < 0:
        #print ("Reflection detected")
        Vt[2, :] *= -1
        R = np.dot(Vt.T, U.T)


#    if scale:
#        varA = np.var(A, axis=0).sum()
#        c = 1 / (1 / varA * np.sum(S))  # scale factor
#        t = -R * (centroid_B.T * c) + centroid_A.T
#    else:
#    c = 1
    t = np.dot( -R, centroid_A.T) + centroid_B.T

    
    match_target = np.zeros((4,4))
    match_target[:3,:3] = R
    match_target[0,3] = t[0]
    match_target[1,3] = t[1]
    match_target[2,3] = t[2]
    match_target[3,3] = 1

    Diff = (np.dot(R,AA.T) - BB.T).T
    lrms = 0
    for iii in range(np.shape(A)[0]):
        #rms = rms + Diff[iii,0] * Diff[iii,0] + Diff[iii,1] * Diff[iii,1] + Diff[iii,2] * Diff[iii,2] 
        lrms = lrms + np.dot(Diff[iii,:] , Diff[iii,:]) 
    lrms = lrms/N
    lrms = np.sqrt(lrms)
    
    return match_target, lrms, Diff


def points_rmse(Ref, Cur):
    
    import numpy as np
    import sklearn.metrics
    import math
    
    rms = 0
    N=Ref.shape[0]
    M=Cur.shape[0]
    if M != N:
        return 0
    
    Diff = Cur-Ref
    
    for iii in range(N):
        #rms = rms + Diff[iii,0] * Diff[iii,0] + Diff[iii,1] * Diff[iii,1] + Diff[iii,2] * Diff[iii,2] 
        # dot product is equivalent
        rms = rms + np.dot(Diff[iii,:] , Diff[iii,:]) 
    rmse = sqrt(rms/N)
    
    # this is not correct for points, at least as it is...
    #mse = sklearn.metrics.mean_squared_error(Ref, Cur)
    #rmse = math.sqrt(mse)
    
    return rmse